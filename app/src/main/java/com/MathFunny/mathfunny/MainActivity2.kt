package com.MathFunny.mathfunny

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.log

public var correct: Int = 0
public var incorrect: Int = 0
public var isSelectAnswer = false


class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val intent = intent
        if (intent.hasExtra("selectGame")) {
            val selectGame = intent.getStringExtra("selectGame")?.toString()
            play(selectGame)
        }
        val btnBack = findViewById<Button>(R.id.btnBack)
        btnBack.setOnClickListener {
            val intent = Intent(MainActivity2@this, MainActivity::class.java)
            startActivity(intent)
        }
    }

    fun play(selectGame: String?) {
        val textNumber1 = findViewById<TextView>(R.id.number1)
        val textNumber2 = findViewById<TextView>(R.id.number2)
        val txtStatic = findViewById<TextView>(R.id.txttrue)
        val txtOperator = findViewById<TextView>(R.id.plus)
        when (selectGame) {
            "1" -> {
                txtOperator.text = "+"
            }
            "2" -> {
                txtOperator.text = "-"
            }
            else -> {
                txtOperator.text = "x"
            }
        }

        val btnAns1 = findViewById<Button>(R.id.btnAns1)
        val btnAns2 = findViewById<Button>(R.id.btnAns2)
        val btnAns3 = findViewById<Button>(R.id.btnAns3)
        val btnPlay = findViewById<Button>(R.id.btnAgain)
        btnPlay.visibility = View.GONE
        isSelectAnswer = false

        val txtShow = findViewById<TextView>(R.id.txtShowAns)

        textNumber1.text = (0..10).random().toString()
        textNumber2.text = (0..10).random().toString()

        val ran = (1..3).random().toString()

        val num1 = (textNumber1).text.toString().toInt()
        val num2 = (textNumber2).text.toString().toInt()

        var randomNum1: String
        var randomNum2: String
        var randomNum3: String

        when (selectGame) {
            "1" -> {
                randomNum1 = ((num1 +num2).toString())
                do {
                    randomNum2 = (1..20).random().toString()
                } while (randomNum2 == (num1 + num2).toString())
                do {
                    randomNum3 = (1..20).random().toString()
                } while (randomNum3 == (num1 + num2).toString() || randomNum3 == randomNum2)
            }
            "2" -> {
                randomNum1 = ((num1 - num2).toString())
                do {
                    randomNum2 = (1..10).random().toString()
                } while (randomNum2 == (num1 - num2).toString())
                do {
                    randomNum3 = (1..10).random().toString()
                } while (randomNum3 == (num1 - num2).toString() || randomNum3 == randomNum2)
            }
            else -> {
                randomNum1 = ((num1 * num2).toString())
                do {
                    randomNum2 = (1..100).random().toString()
                } while (randomNum2 == (num1 * num2).toString())
                do {
                    randomNum3 = (1..100).random().toString()
                } while (randomNum3 == (num1 * num2).toString() || randomNum3 == randomNum2)
            }
        }

        when (ran) {
            "1" -> {
                btnAns1.text = (randomNum1.toInt()).toString()
                btnAns2.text = (randomNum2.toInt()).toString()
                btnAns3.text = (randomNum3.toInt()).toString()
            }
            "2" -> {
                btnAns2.text = (randomNum1.toInt()).toString()
                btnAns1.text = (randomNum2.toInt()).toString()
                btnAns3.text = (randomNum3.toInt()).toString()
            }
            else -> {
                btnAns3.text = (randomNum1.toInt()).toString()
                btnAns1.text = (randomNum2.toInt()).toString()
                btnAns2.text = (randomNum3.toInt()).toString()
            }
        }

        btnAns1.setBackgroundColor(Color.GRAY)
        btnAns2.setBackgroundColor(Color.GRAY)
        btnAns3.setBackgroundColor(Color.GRAY)

        btnAns1.setOnClickListener {
            if (ran == "1") {

                if(!isSelectAnswer){
                    btnAns1.setBackgroundColor(Color.GREEN)
                    txtShow.text = "CORRECT"
                    correct++
                    txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                    btnAgain.visibility = View.VISIBLE
                    isSelectAnswer =  true

                }

            } else {

                if(!isSelectAnswer){
                    btnAns1.setBackgroundColor(Color.RED)
                    txtShow.text = "INCORRECT"
                    incorrect++
                    txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                    btnAgain.visibility = View.VISIBLE
                    isSelectAnswer =  true

                }
            }
        }
        btnAns2.setOnClickListener {
            if (ran == "2") {

                if(!isSelectAnswer){
                    btnAns2.setBackgroundColor(Color.GREEN)
                    txtShow.text = "CORRECT"
                    correct++
                    txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                    btnAgain.visibility = View.VISIBLE
                    isSelectAnswer =  true

                }
            } else {


                if(!isSelectAnswer){
                    btnAns2.setBackgroundColor(Color.RED)
                    txtShow.text = "INCORRECT"
                    incorrect++
                    txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                    btnAgain.visibility = View.VISIBLE
                    isSelectAnswer =  true
                }
            }
        }
        btnAns3.setOnClickListener {
            if (ran == "3") {

                if(!isSelectAnswer){
                    btnAns3.setBackgroundColor(Color.GREEN)
                    txtShow.text = "CORRECT"
                    correct++
                    txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                    btnAgain.visibility = View.VISIBLE
                    isSelectAnswer =  true

                }
            } else {

                if(!isSelectAnswer){
                    btnAns3.setBackgroundColor(Color.RED)
                    txtShow.text = "INCORRECT"
                    incorrect++
                    txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                    btnAgain.visibility = View.VISIBLE
                    isSelectAnswer =  true

                }
            }
        }
        btnPlay.setOnClickListener {
            play(selectGame)
        }
    }
}